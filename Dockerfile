FROM node:9-slim

WORKDIR /usr/src/app

ENV PORT 8081

COPY package*.json ./
RUN npm install

COPY . .

EXPOSE 8081
CMD ["npm", "start"]