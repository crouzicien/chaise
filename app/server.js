// IMPORTS
const cors = require('cors'),
    Joi = require('joi'),
    jsonParser = require('body-parser').json(),
    express = require('express'),
    app = express(),
    mongoose = require('mongoose');

// CONFIGURATION DE MONGODB
let mongoAdress = process.env.MONGO_ADDRESS || 'localhost'
let mongoPort = process.env.MONGO_PORT || '27017'
let mongoBase = process.env.MONGO_BASE || 'test'
let mongoUser = process.env.MONGO_USERNAME || ''
let mongoPass = process.env.MONGO_PASS || ''
let mongoAuth = mongoUser != '' ? `${mongoUser}:${mongoPass}@` : ''
let mongoUrl = `mongodb://${mongoAuth}${mongoAdress}:${mongoPort}/${mongoBase}`;



mongoose.connect(mongoUrl, {
    useNewUrlParser: true
});
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Erreur lors de la connexion sur', mongoUrl));
db.once('open', () => {
    console.log("Connexion à la base OK");
    db.createCollection("convive")
});








// CONFIGURATION WEB SERVER
app.use(cors())
app.use(jsonParser);
app.use((req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    next();
})



// DATA
const convives = [];
const port = 8081;


// SCHEMA COMPTE
const convive_model = Joi.object().keys({
    nom: Joi.string().required(),
    prenom: Joi.string().required()
});



// ROUTES
app.get('/list', async (req, res) => {
    let store = await db.collection('convive').find({}).toArray()
    res.json({
        result: store,
        message: `${store.length} convive(s).`
    })
});

app.post('/add', (req, res) => {
    let data = req.body

    // Analyse de la donnée
    Joi.validate(data, convive_model, async (err, value) => {

        if (err) {
            // Données invalides
            res.status(422).json({
                result: 0,
                message: 'Invalid request data'
            });

        } else {
            // Données valides
            await db.collection('convive').insertOne(data)
            res.json({
                result: 1,
                message: `Le client ${data.nom} ${data.prenom} à bien été ajouté.`
            });
        }
    });
});

app.get('/flush', async (req, res) => {
    await db.collection('convive').deleteMany({})
    convives.length = 0
    res.json({
        result: convives,
        message: `${convives.length} convive(s).`
    })
});

app.get('/help', (req, res) => {
    res.json({
        message: `POST /add - Add a data\nGET /list - List all data\nGET /flush - Delete all data`
    })
});


// START SERVER
app.listen(port, () => {
    console.log("Mon serveur fonctionne sur http://localhost:" + port);
});









let gracefulExit = function() { 
    mongoose.connection.close(function () {
      console.log('Mongoose default connection with DB :' + mongoUrl + ' is disconnected through app termination');
      process.exit(0);
    });
  }

// If the connection throws an error
mongoose.connection.on("error", function (err) {
    console.error('Failed to connect to DB ' + mongoUrl + ' on startup ', err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection to DB :' + mongoUrl + ' disconnected');
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', gracefulExit).on('SIGTERM', gracefulExit);
